Julia Kroll and Tristan Leigh readme.txt

In our EncodingHelper program, we decided to allow more flexible input
formatting for codepoints and bytes. Therefore, our program will still behave
the same way given codepoint inputs such as:

Input: EncodingHelper -i codepoint -o string U+0061
Output: a

Input: EncodingHelper -i codepoint -o string 0061
Output: a

Input: EncodingHelper -i codepoint -o string 61
Output: a

Input: EncodingHelper -i codepoint -o string \u0061
Output: a

Input: EncodingHelper -i codepoint -o string u0061
Output: a

etc... And similarly, for utf8 byte inputs:

Input: EncodingHelper -i utf8 -o codepoint \xC3\xA9
Output: U+00E9

Input: EncodingHelper -i utf8 -o codepoint C3A9
Output: U+00E9

Input: EncodingHelper -i utf8 -o codepoint xC3xA9
Output: U+00E9

etc...
