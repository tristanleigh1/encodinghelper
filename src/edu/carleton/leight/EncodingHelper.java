package edu.carleton.leight;

import java.io.FileNotFoundException;

/**
 * Created by leight on 4/11/15.
 */
public class EncodingHelper {

    private EncodingHelperChar[] EHChars;
    private String inputType;
    private String outputType;
    private EncodingHelperConstructor EHConstructor;



    public EncodingHelper(String input, String inFlag, String outFlag) {
        EHConstructor = new EncodingHelperConstructor();
        if (inFlag.toLowerCase().equals("string") || inFlag.equals("none")) {
            inputType = "string";
            EHChars = EHConstructor.stringConstructor(input);
        }
        else if (inFlag.toLowerCase().equals("codepoint")) {
            inputType = "codepoint";
            EHChars = EHConstructor.codePointConstructor(input);
        }
        else if (inFlag.toLowerCase().equals("utf8")) {
            inputType = "utf8";
            EHChars = EHConstructor.utf8Constructor(input);
        }
        else { // invalid input type
            System.out.println(inFlag + " is an invalid input type");
            System.out.println(getUseMsg());
            System.exit(1);
        }
        if (outFlag.toLowerCase().equals("string")) {
            outputType = "string";
        }
        else if (outFlag.toLowerCase().equals("codepoint")) {
            outputType = "codepoint";
        }
        else if (outFlag.toLowerCase().equals("utf8")) {
            outputType = "utf8";
        }
        else if (outFlag.toLowerCase().equals("summary") ||
                (outFlag.equals("none"))) {
            outputType = "summary";
        }
        else { // invalid output type
            System.out.println(outFlag + " is an invalid output type");
            System.out.println(getUseMsg());
            System.exit(1);
        }
    }

    public String getOutputType() {
        return this.outputType;
    }

    public String getInputType() {
        return this.inputType;
    }

    public String getUseMsg() {
        String usgMsg = "usage: "
                + "[<input>] [-i|--input <input type>] "
                + "[-o|--output <output type>]\n"
                + "Input is mandatory; input and output flags are optional\n"
                + "Default types are string input and summary output\n"
                + "Arguments in square brackets above can be "
                + "rearranged in any order\n"
                + "Input types: string, codepoint, utf8\n"
                + "No spaces between characters or UTF-8 bytes; optional spaces"
                + " between codepoints\n"
                + "Output types: summary, string, codepoint, utf8\n";
        return usgMsg;
    }

    public String getSummaryOutput() {
        String summaryString = "";
        if (EHChars.length == 1) { // summary for 1 char
            String characterString = "Character: ";
            char unicodeChar = (char) EHChars[0].getCodePoint();
            String charAsString = Character.toString(unicodeChar);
            characterString += charAsString;
            String codePointString = "Code point: ";
            codePointString += EHChars[0].toCodePointString();
            String nameString = "Name: ";
            try {
                nameString += EHChars[0].getCharacterName();
            }
            catch (FileNotFoundException e) {
                nameString += "(unicode data file not found)";
            }
            String utf8String = "UTF8: " + EHChars[0].toUtf8String();
            summaryString = characterString + "\n" + codePointString
                    + "\n" + nameString + "\n" + utf8String + "\n";
        }
        else { // summary for 2+ chars
            String stringString = "String: ";
            for (int i = 0; i < EHChars.length; i++) {
                char unicodeChar = (char) EHChars[i].getCodePoint();
                String charAsString = Character.toString(unicodeChar);
                stringString += charAsString;
            }
            String codePointString = "Code points: ";
            for (int i = 0; i < EHChars.length; i++) {
                codePointString += EHChars[i].toCodePointString() + " ";
            }
            String utf8String = "UTF8: ";
            for (int i = 0; i < EHChars.length; i++) {
                utf8String += EHChars[i].toUtf8String();
            }
            summaryString = stringString + "\n" + codePointString + "\n"
                            + utf8String + "\n";
        }
        return summaryString;
    }

    public String getCodePointOutput() {
        String codePointString = "";
        for (int i = 0; i < EHChars.length; i++) {
            if (EHChars.length == 1) {
                codePointString += EHChars[i].toCodePointString();
            }
            else {
                codePointString += EHChars[i].toCodePointString() + " ";
            }
        }
        return codePointString;
    }

    public String getStringOutput() {
        String stringString = "";
        for (int i = 0; i < EHChars.length; i++) {
            char unicodeChar = (char) EHChars[i].getCodePoint();
            String charAsString = Character.toString(unicodeChar);
            stringString += charAsString;
        }
        return stringString;
    }

    public String getUTF8Output() {
        String utf8String = "";
        for (int i = 0; i < EHChars.length; i++) {
            utf8String += EHChars[i].toUtf8String();
        }
        return utf8String;
    }


    public static void main (String args[]) {

        String input = "";
        String inFlag = "none";
        String outFlag = "none";
        int argsCounter = 0;
        // loop through args once to parse input and output flags
        while (argsCounter < args.length) {
            String currentArg = args[argsCounter];
            if ((currentArg.equals("-i") || currentArg.equals("--input"))
                    && (argsCounter + 1 < args.length)
                    && (inFlag.equals("none"))) {
                inFlag = args[argsCounter + 1];
                argsCounter += 2;
            }
            else if ((currentArg.equals("-o")||currentArg.equals("--output"))
                    && (argsCounter + 1 < args.length)
                    && (outFlag.equals("none"))) {
                outFlag = args[argsCounter + 1];
                argsCounter += 2;
            }
            else {
                argsCounter++;
            }
        }
        argsCounter = 0;
        int prevCodeptIndex = -1;
        // loop through args again to parse data input
        while (argsCounter < args.length) {
            if (args[argsCounter].equals("-i")
             || args[argsCounter].equals("--input")
             || args[argsCounter].equals("-o")
             || args[argsCounter].equals("--output")) {
                argsCounter += 2;
            }
            // Ensure that for codepoint input, there is either a single
            // codepoint, or adjacent codepoints
            // (multiple non-adjacent codepoints are not allowed)
            else if (inFlag.equals("codepoint")
             && (prevCodeptIndex == -1 || prevCodeptIndex == argsCounter - 1)) {
                input += args[argsCounter] + " "; // add spaces between codepts
                prevCodeptIndex = argsCounter;
                argsCounter++;
            }
            else if (input.equals("")){
                input += args[argsCounter];
                argsCounter++;
            }
            else {
                System.out.println("Input data is formatted incorrectly");
                System.out.println("usage: "
                        + "[<input>] [-i|--input <input type>] "
                        + "[-o|--output <output type>]\nInput is mandatory;"
                        + " input and output flags are optional\n"
                        + "Default types are string input and summary output\n"
                        + "Arguments in square brackets above can be "
                        + "rearranged in any order\n"
                        + "Input types: string, codepoint, utf8\n"
                        + "No spaces between characters or UTF-8 bytes; "
                        + "optional spaces between codepoints\n"
                        + "Output types: summary, string, codepoint, utf8\n");
                System.exit(1);
            }
        }
        EncodingHelper EH = new EncodingHelper(input, inFlag, outFlag);
        if (EH.getOutputType().equals("summary")) {
            System.out.println(EH.getSummaryOutput());

        }
        else if (EH.getOutputType().equals("codepoint")) {
            System.out.println(EH.getCodePointOutput());
        }
        else if (EH.getOutputType().equals("string")) {
            System.out.println(EH.getStringOutput());
        }
        else { // output type = utf8
            System.out.println(EH.getUTF8Output());
        }


    }



}
