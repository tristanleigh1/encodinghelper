package edu.carleton.leight;
import java.lang.*;
import java.io.*;
import java.util.*;
import java.util.Scanner;

/**
 * The main model class for the EncodingHelper project in
 * CS 257, Spring 2015, Carleton College. Each object of this class
 * represents a single Unicode character. The class's methods
 * generate various representations of the character.
 */
public class EncodingHelperChar {
    private int codePoint;

    public EncodingHelperChar(int codePoint) {
        if (codePoint >= 0 && codePoint <= 0x10FFFF) {
            this.codePoint = codePoint;
        }
        else {
            String errorMessage = "ERROR: Codepoint must be between 0 and 0x10FFFF";
            throw new IllegalArgumentException(errorMessage);
        }
    }

    public EncodingHelperChar(byte[] utf8Bytes) {
        int arrayLength = utf8Bytes.length;
        int codePt;
        if (arrayLength == 1) {
            byte errorCheckByteMask = (byte) 0b10000000;
            byte errorCheckByteAnd = (byte) (utf8Bytes[0] & errorCheckByteMask);
            byte validByte = 0b00000000;
            if (errorCheckByteAnd != validByte) {
                String byteErrorMessage = "ERROR: Invalid one-byte byte array";
                throw new IllegalArgumentException(byteErrorMessage);
            }
            else { // valid byte
                codePt = utf8Bytes[0];
            }
        }
        else if (arrayLength == 2) {
            // check for valid high byte
            byte errorCheckHighByteMask = (byte) 0b11100000;
            byte errorCheckHighByteAnd = (byte) (utf8Bytes[0] & errorCheckHighByteMask);
            byte validHighByte = (byte) 0b11000000;
            // check for valid low byte
            byte errorCheckLowByteMask = (byte) 0b11000000;
            byte errorCheckLowByteAnd = (byte) (utf8Bytes[1] & errorCheckLowByteMask);
            byte validLowByte = (byte) 0b10000000;
            if ((errorCheckHighByteAnd != validHighByte) ||
                (errorCheckLowByteAnd != validLowByte)) {
                String byteErrorMessage = "ERROR: Invalid two-byte byte array";
                throw new IllegalArgumentException(byteErrorMessage);
            }
            else{ // valid two bytes
                byte highByteMask = 0b00011111;
                byte lowByteMask = 0b00111111;
                byte highByteAnd = (byte) (utf8Bytes[0] & highByteMask);
                byte lowByteAnd = (byte) (utf8Bytes[1] & lowByteMask);
                int highByteInt = (int) highByteAnd;
                int lowByteInt = (int) lowByteAnd;
                highByteInt = highByteInt << 6;
                codePt = highByteInt | lowByteInt;
            }
        }
        else if (arrayLength == 3) {
            // check for valid high byte
            byte errorCheckHighByteMask = (byte) 0b11110000;
            byte errorCheckHighByteAnd = (byte) (utf8Bytes[0] & errorCheckHighByteMask);
            byte validHighByte = (byte) 0b11100000;
            // check for valid lower two bytes
            byte errorCheckMidLowByteMask = (byte) 0b11000000;
            byte validMidLowByte = (byte) 0b10000000;
            byte errorCheckMidByteAnd = (byte) (utf8Bytes[1] & errorCheckMidLowByteMask);
            byte errorCheckLowByteAnd = (byte) (utf8Bytes[2] & errorCheckMidLowByteMask);
            if ((errorCheckHighByteAnd != validHighByte) ||
                (errorCheckMidByteAnd != validMidLowByte) ||
                (errorCheckLowByteAnd != validMidLowByte)) {
                String byteErrorMessage = "ERROR: Invalid three-byte byte array";
                throw new IllegalArgumentException(byteErrorMessage);
            }
            else { // valid three bytes
                byte highByteMask = 0b00001111;
                byte midByteMask = 0b00111111;
                byte lowByteMask = 0b00111111;
                byte highByteAnd = (byte) (utf8Bytes[0] & highByteMask);
                byte midByteAnd = (byte) (utf8Bytes[1] & midByteMask);
                byte lowByteAnd = (byte) (utf8Bytes[2] & lowByteMask);
                int highByteInt = (int) highByteAnd;
                int midByteInt = (int) midByteAnd;
                int lowByteInt = (int) lowByteAnd;
                highByteInt = highByteInt << 12;
                midByteInt = midByteInt << 6;
                codePt = highByteInt | midByteInt | lowByteInt;
            }
        }
        else if (arrayLength == 4) {
            // check for valid high byte
            byte errorCheckHighByteMask = (byte) 0b11111000;
            byte errorCheckHighByteAnd = (byte) (utf8Bytes[0] & errorCheckHighByteMask);
            byte validHighByte = (byte) 0b11110000;
            // check for valid lower three bytes
            byte errorCheckLowerByteMask = (byte) 0b11000000;
            byte validLowerByte = (byte) 0b10000000;
            byte errorCheckMidByteAnd = (byte) (utf8Bytes[1] & errorCheckLowerByteMask);
            byte errorCheckLowByteAnd = (byte) (utf8Bytes[2] & errorCheckLowerByteMask);
            byte errorCheckLastByteAnd = (byte) (utf8Bytes[3] & errorCheckLowerByteMask);
            if ((errorCheckHighByteAnd != validHighByte) ||
                (errorCheckMidByteAnd != validLowerByte) ||
                (errorCheckLowByteAnd != validLowerByte) ||
                (errorCheckLastByteAnd != validLowerByte)) {
                String byteErrorMessage = "ERROR: Invalid four-byte byte array";
                throw new IllegalArgumentException(byteErrorMessage);
            }
            else{ // valid four bytes
                byte highByteMask = 0b00000111;
                byte midByteMask = 0b00111111;
                byte lowByteMask = 0b00111111;
                byte lastByteMask = 0b00111111;
                byte highByteAnd = (byte) (utf8Bytes[0] & highByteMask);
                byte midByteAnd = (byte) (utf8Bytes[1] & midByteMask);
                byte lowByteAnd = (byte) (utf8Bytes[2] & lowByteMask);
                byte lastByteAnd = (byte) (utf8Bytes[3] & lastByteMask);
                int highByteInt = (int) highByteAnd;
                int midByteInt = (int) midByteAnd;
                int lowByteInt = (int) lowByteAnd;
                int lastByteInt = (int) lastByteAnd;
                highByteInt = highByteInt << 18;
                midByteInt = midByteInt << 12;
                lowByteInt = lowByteInt << 6;
                codePt = highByteInt | midByteInt | lowByteInt | lastByteInt;
            }
        }
        else {
            String byteSizeErrorMessage =
            "ERROR: byte array cannot contain more than 4 bytes";
            throw new IllegalArgumentException(byteSizeErrorMessage);
        }
        if (codePt > 0x10FFFF) {
            String codePointErrorMessage =
            "ERROR: Code point exceeds highest valid code point of 0x10FFFF";
            throw new IllegalArgumentException(codePointErrorMessage);
        }
        if (codePt < 0) {
            String codePointErrorMessage =
            "ERROR: Code point cannot be a negative value";
            throw new IllegalArgumentException(codePointErrorMessage);
        }
        this.codePoint = codePt;
    }

    public EncodingHelperChar(char ch) {
        int cdPoint;
        String chString = Character.toString(ch);
        cdPoint = Character.codePointAt(chString,0);
        this.codePoint = cdPoint;
    }

    public int getCodePoint() {

        return this.codePoint;
    }

    public void setCodePoint(int codePoint) {

        this.codePoint = codePoint;
    }

    /**
     * Converts this character into an array of the bytes in its UTF-8
     * representation.
     *   For example, if this character is a lower-case letter e with an acute
     * accent, whose UTF-8 form consists of the two-byte sequence C3 A9, then
     * this method returns a byte[] of length 2 containing C3 and A9.
     *
     * @return the UTF-8 byte array for this character
     */
    public byte[] toUtf8Bytes() {
        int codePt = getCodePoint();
        byte[] byteArrSizeOne = new byte[1];
        byte[] byteArrSizeTwo = new byte[2];
        byte[] byteArrSizeThree = new byte[3];
        byte[] byteArrSizeFour = new byte[4];
        byte[][] encodingsArr = new byte[][]{byteArrSizeOne, byteArrSizeTwo,
                                            byteArrSizeThree, byteArrSizeFour};
        int encodingsArrIndex;
        if (codePt <= 0x7F) { // one byte array
            byteArrSizeOne[0] = (byte) codePt;
            encodingsArrIndex = 0;
        }
        else if (codePt >= 0x80 && codePt <= 0x7FF) { //two byte array
            int highByteMask = 0b11111000000;
            int lowByteMask = 0b111111;
            int highByteAnd = codePt & highByteMask;
            int lowByteAnd = codePt & lowByteMask;
            highByteAnd = highByteAnd >> 6;
            int highByteUTF8bits = 0b11000000;
            int lowByteUTF8bits = 0b10000000;
            int highByteOr = highByteAnd | highByteUTF8bits;
            int lowByteOr = lowByteAnd | lowByteUTF8bits;
            byte highByte = (byte) highByteOr;
            byte lowByte = (byte) lowByteOr;
            byteArrSizeTwo[0] = highByte;
            byteArrSizeTwo[1] = lowByte;
            encodingsArrIndex = 1;
        }
        else if ((codePt >= 0x800 && codePt <= 0xD7FF) ||
                (codePt >= 0xE000 && codePt <= 0xFFFF)) { // three byte array
            int highByteMask = 0b1111000000000000;
            int midByteMask = 0b111111000000;
            int lowByteMask = 0b111111;
            int highByteAnd = codePt & highByteMask;
            int midByteAnd = codePt & midByteMask;
            int lowByteAnd = codePt & lowByteMask;
            highByteAnd = highByteAnd >> 12;
            midByteAnd = midByteAnd >> 6;
            int highByteUTF8bits = 0b11100000;
            int midLowByteUTF8bits = 0b10000000;
            int highByteOr = highByteAnd | highByteUTF8bits;
            int midByteOr = midByteAnd | midLowByteUTF8bits;
            int lowByteOr = lowByteAnd | midLowByteUTF8bits;
            byte highByte = (byte) highByteOr;
            byte midByte = (byte) midByteOr;
            byte lowByte = (byte) lowByteOr;
            byteArrSizeThree[0] = highByte;
            byteArrSizeThree[1] = midByte;
            byteArrSizeThree[2] = lowByte;
            encodingsArrIndex = 2;
        }
        else { // then codePt >= 0x10000 && codePt <= 0x10FFFF, four byte array
            int highByteMask = 0b111000000000000000000;
            int midByteMask = 0b111111000000000000;
            int lowByteMask = 0b111111000000;
            int lastByteMask = 0b111111;
            int highByteAnd = codePt & highByteMask;
            int midByteAnd = codePt & midByteMask;
            int lowByteAnd = codePt & lowByteMask;
            int lastbyteAnd = codePt & lastByteMask;
            highByteAnd = highByteAnd >> 18;
            midByteAnd = midByteAnd >> 12;
            lowByteAnd = lowByteAnd >> 6;
            int highByteUTF8bits = 0b11110000;
            int midLowByteUTF8bits = 0b10000000;
            int highByteOr = highByteAnd | highByteUTF8bits;
            int midByteOr = midByteAnd | midLowByteUTF8bits;
            int lowByteOr = lowByteAnd | midLowByteUTF8bits;
            int lastByteOr = lastbyteAnd | midLowByteUTF8bits;
            byte highByte = (byte) highByteOr;
            byte midByte = (byte) midByteOr;
            byte lowByte = (byte) lowByteOr;
            byte lastByte = (byte) lastByteOr;
            byteArrSizeFour[0] = highByte;
            byteArrSizeFour[1] = midByte;
            byteArrSizeFour[2] = lowByte;
            byteArrSizeFour[3] = lastByte;
            encodingsArrIndex = 3;
        }
        return encodingsArr[encodingsArrIndex];
    }

    /**
     * Generates the conventional 4-digit hexadecimal code point notation for
     * this character.
     *   For example, if this character is a lower-case letter e with an acute
     * accent, then this method returns the string U+00E9 (no quotation marks in
     * the returned String).
     *
     * @return the U+ string for this character
     */
    public String toCodePointString() {
        int codePt = this.getCodePoint();
        int hexLength = Integer.toHexString(codePt).length();
        int numPaddedZeros = 4 - hexLength; // pads codepoints to be 4 digits
        String paddedZeros = "";
        // for rare codepoints of length 5 or 6, numPaddedZeros is negative,
        // so do not pad string with zeros.
        if (numPaddedZeros > 0) {
            for (int i = 0; i < numPaddedZeros; i++) {
                paddedZeros += "0";
            }
        }
        String unicodeString = "U+" + paddedZeros + Integer.toHexString(codePt);
        unicodeString = unicodeString.toUpperCase();
        return unicodeString;
    }

    /**
     * Generates a hexadecimal representation of this character suitable for
     * pasting into a string literal in languages that support hexadecimal byte
     * escape sequences in string literals (e.g. C, C++, and Python).
     *   For example, if this character is a lower-case letter e with an acute
     * accent (U+00E9), then this method returns the string \xC3\xA9. Note that
     * quotation marks should not be included in the returned String.
     *
     * @return the escaped hexadecimal byte string
     */
    public String toUtf8String() {
        int codePt = this.getCodePoint();
        byte[] byteArr = this.toUtf8Bytes();
        String byteArrStr = "";
        for (int i = 0; i < byteArr.length; i++) {
            String slashX = "\\x";
            String hexStr = Integer.toHexString(byteArr[i]);
            if (hexStr.length() > 1) {
                hexStr = hexStr.substring(hexStr.length() - 2).toUpperCase();
            }
            else {
                hexStr = "0" + hexStr.toUpperCase();
            }
            byteArrStr += slashX + hexStr;
        }
        return byteArrStr;
    }

    /**
     * Generates the official Unicode name for this character.
     *   For example, if this character is a lower-case letter e with an acute
     * accent, then this method returns the string "LATIN SMALL LETTER E WITH
     * ACUTE" (without quotation marks).
     *
     * @return this character's Unicode name
     */
    public String getCharacterName() throws FileNotFoundException {
        int codePt = this.getCodePoint();
        int hexLength = Integer.toHexString(codePt).length();
        int numPaddedZeros = 4 - hexLength; // pads codepoint to be four digits.
        String paddedZeros = "";
        // for rare codepoints of length 5 or 6, numPaddedZeros is negative,
        // so do not pad string with zeros.
        if (numPaddedZeros > 0) {
            for (int i = 0; i < numPaddedZeros; i++) {
                paddedZeros += "0";
            }
        }
        String unicodeString = paddedZeros + Integer.toHexString(codePt);
        unicodeString = unicodeString.toUpperCase();

        String characterName = "";
        Scanner fileReader = new Scanner(new File("edu/carleton/leight/UnicodeData.txt"));
        // src/edu/carleton/leight/UnicodeData.txt
        String currentLine;
        boolean foundLine = false;
        while ((fileReader.hasNextLine()) && !foundLine) {
            currentLine = fileReader.nextLine();
            if (currentLine.substring(0,unicodeString.length()).equals(unicodeString)) {
                foundLine = true;
                String[] partsOfLine = currentLine.split(";");
                characterName = partsOfLine[1];
                if (characterName.equals("<control>")) {
                    characterName += " ";
                    if (partsOfLine.length >= 11) {
                        characterName += partsOfLine[10];
                    }
                    else {
                        characterName += "(no name)";
                    }
                }
            }
            else  {
                characterName = "<unknown> " + "U+" + unicodeString;
            }
        }
        fileReader.close();
        return characterName;
    }

}
