package edu.carleton.leight;

import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.*;

/**
 * Created by leight and krollj on 4/4/15.
 */
public class EncodingHelperCharTest {

    /** Tests that a given code point is between 0x0000 and 0x10FFFF.
     */
    @Test
    public void testCodePointConstructorShouldBeValidCodePoint() throws Exception {
        EncodingHelperChar aChar = new EncodingHelperChar(0x55);
        assertTrue(aChar.getCodePoint() >= 0);
        assertTrue(aChar.getCodePoint() <= 0x10FFFF);
    }

    /** Tests that a code point of a given byte array is between 0x0000 and
     * 0x10FFFF. Also tests if utf-8 byte constructor converts the byte array
     * to the correct code point.
     */
    @Test
    public void testUTF8ConstructorShouldBeValidByteArray() throws Exception {
        byte[] byteArr = new byte[3];
        byteArr[0] = (byte) 0xe5;
        byteArr[1] = (byte) 0x94;
        byteArr[2] = (byte) 0xa4;
        EncodingHelperChar aChar = new EncodingHelperChar(byteArr);
        assertTrue(aChar.getCodePoint() >= 0);
        assertTrue(aChar.getCodePoint() <= 0x10FFFF);
        int expectedCodePoint = 0x5524;
        assertEquals(expectedCodePoint, aChar.getCodePoint());
    }

    /** Tests that a code point of a given character is between 0x0000 and
     * 0x10FFFF. Also tests if character constructor converts the character to
     * the correct code point.
     */
    @Test
    public void testCharConstructorShouldBeValidChar() throws Exception {
        char ch = 'a';
        EncodingHelperChar aChar = new EncodingHelperChar(ch);
        assertTrue(aChar.getCodePoint() >= 0);
        assertTrue(aChar.getCodePoint() <= 0x10FFFF);
        int expectedCodePoint = 0x61;
        assertEquals(expectedCodePoint, aChar.getCodePoint());
    }

    /** Tests that getCodePoint() correctly returns the stored code point.
     */
    @Test
    public void testGetCodePointShouldReturnCodePoint() throws Exception {
        EncodingHelperChar aChar = new EncodingHelperChar(0x55);
        int storedCodePoint = aChar.getCodePoint();
        int expected = 0x55;
        assertEquals(expected, storedCodePoint);
    }

    /** Tests that setCodePoint() does not set code point to a negative value.
     */
    @Test
    public void testSetCodePointShouldReturnNonNegative() throws Exception {
        EncodingHelperChar aChar = new EncodingHelperChar(0x55);
        int newCodePoint = 0x1035;
        aChar.setCodePoint(newCodePoint);
        assertTrue(aChar.getCodePoint() >= 0);
    }

    /** Tests that setCodePoint() does not set code point to a value exceeding
     * 0x10FFFF.
     */
    @Test
    public void testSetCodePointShouldNotExceed0x10FFFF() throws Exception {
        EncodingHelperChar aChar = new EncodingHelperChar(0x55);
        int newCodePoint = 0x23562;
        aChar.setCodePoint(newCodePoint);
        assertTrue(aChar.getCodePoint() <= 0x10FFFF);
    }

    /** Tests that toUtf8Bytes() correctly converts the stored code point to a
     * byte array.
     */
    @Test
    public void testToUtf8Bytes() throws Exception {
        EncodingHelperChar aChar = new EncodingHelperChar(0x5524);
        byte[] charToByteArr = aChar.toUtf8Bytes();
        byte[] expected = new byte[3];
        expected[0] = (byte) 0xe5;
        expected[1] = (byte) 0x94;
        expected[2] = (byte) 0xa4;
        assertEquals(Arrays.toString(expected), Arrays.toString(charToByteArr));
    }

    /** Tests that toCodePointString() correctly converts the stored code point
     * of a character to a 4-digit hexadecimal code point notation.
     */
    @Test
    public void testToCodePointString() throws Exception {
        EncodingHelperChar aChar = new EncodingHelperChar(0x55);
        String codePointString = aChar.toCodePointString();
        String expected = "U+0055";
        assertEquals(expected, codePointString);
    }

    /** Tests that toUtf8String() correctly converts the stored code point of a
     * character to a string hexadecimal representation of this character.
     */
    @Test
    public void testToUtf8String() throws Exception {
        EncodingHelperChar aChar = new EncodingHelperChar(0x55);
        String utf8String = aChar.toUtf8String();
        String expected = "\\x55";
        assertEquals(expected, utf8String);
    }

    /** Tests that getCharacterName() correctly converts the stored code point
     * of a character to the official Unicode name for that character in a
     * string.
     */
    @Test
    public void testGetCharacterName() throws Exception {
        EncodingHelperChar aChar = new EncodingHelperChar(0x04CF);
        String characterName = aChar.getCharacterName();
        String expected = "Cyrillic Small Letter Palochka";
        assertEquals(expected, characterName);
    }
}