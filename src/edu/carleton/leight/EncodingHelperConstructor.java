package edu.carleton.leight;

import java.util.IllegalFormatCodePointException;

/**
 * Created by leight on 4/11/15.
 */
public class EncodingHelperConstructor {

    private EncodingHelperChar[] EHCharArray;

    public EncodingHelperChar[] stringConstructor(String input) {
        if (input.length() == 0) {
            System.out.println("No input data found");
            System.out.println(getUseMsg());
        }
        else { // input of 1+ characters
            EHCharArray = new EncodingHelperChar[input.length()];
            for (int i = 0; i < input.length(); i++) {
                char ch = input.charAt(i);
                EncodingHelperChar EHC = new EncodingHelperChar(ch);
                EHCharArray[i] = EHC;
            }

        }
        return EHCharArray;
    }

    public EncodingHelperChar[] codePointConstructor(String input) {
        String[] codePts = input.split(" ");
        EncodingHelperChar[] maxSizeArr = new EncodingHelperChar[codePts.length];
        int codePointsIndex = 0;
        for (int i = 0; i < codePts.length; i++) {
            codePts[i] = codePts[i].replaceAll("[^a-fA-F0-9]", "");
            if (!codePts[i].equals("")) {
                try {
                    int intCodePt = Integer.parseInt(codePts[i], 16);
                    EncodingHelperChar EHC = new EncodingHelperChar(intCodePt);
                    maxSizeArr[codePointsIndex] = EHC;
                    codePointsIndex++;
                }
                catch (IllegalArgumentException e) {
                    System.out.println(codePts[i] + " is an invalid codepoint");
                    System.exit(1);
                }
            }
        }
        if (maxSizeArr.length > codePointsIndex) {
            EHCharArray = new EncodingHelperChar[codePointsIndex];
            for (int i = 0; i < codePointsIndex; i++) {
                EHCharArray[i] = maxSizeArr[i];
            }
        }
        else {
            EHCharArray = maxSizeArr;
        }
        return EHCharArray;
    }

    public EncodingHelperChar[] utf8Constructor(String input) {
        String cleanedBytesString = input.replaceAll("[^a-fA-F0-9]", "");
        int stringLength = cleanedBytesString.length();
        int maxNumEncodings = stringLength/2;
        EncodingHelperChar[] maxArr = new EncodingHelperChar[maxNumEncodings];
        int numEncodings = 0;
        byte[] bytes;
        if (stringLength == 1) {
            bytes = new byte[1];
            String oneHexDigit = cleanedBytesString;
            bytes[0] = (byte) (Character.digit(oneHexDigit.charAt(0), 16));
            EncodingHelperChar EHC = new EncodingHelperChar(bytes[0]);
            EHCharArray = new EncodingHelperChar[1];
            EHCharArray[0] = EHC;
        }
        else if (stringLength > 1 && stringLength % 2 == 0) {
            bytes = new byte[stringLength / 2];
            int numBytes = 0;
            for (int i = 0; i < stringLength; i += 2) {
                String twoHexDigits = cleanedBytesString.substring(i, i + 2);
                int highDigit = Character.digit(twoHexDigits.charAt(0), 16);
                highDigit = highDigit << 4;
                int lowDigit = Character.digit(twoHexDigits.charAt(1), 16);
                bytes[numBytes] = (byte) (highDigit + lowDigit);
                numBytes++;
            }
            int byteArrayIndex = 0;
            int EHCharsIndex = 0;
            while (byteArrayIndex < bytes.length) {
                byte leadingByte = bytes[byteArrayIndex];
                if (((int) leadingByte & 0b10000000) == 0) {
                    // one byte
                    byte[] completeByte = new byte[]{leadingByte};
                    EncodingHelperChar EHC =
                            new EncodingHelperChar(completeByte);
                    maxArr[EHCharsIndex] = EHC;
                    EHCharsIndex++;
                    numEncodings++;
                    byteArrayIndex += 1;
                } else if (((int) leadingByte & 0b11100000) == 0b11000000){
                    // two bytes
                    byte[] completeByte =
                            new byte[]{leadingByte, bytes[byteArrayIndex+1]};
                    EncodingHelperChar EHC =
                            new EncodingHelperChar(completeByte);
                    maxArr[EHCharsIndex] = EHC;
                    EHCharsIndex++;
                    numEncodings++;
                    byteArrayIndex += 2;
                } else if (((int) leadingByte & 0b11110000) == 0b11100000){
                    // three bytes
                    byte[] completeByte = new byte[]
                            {leadingByte,
                                    bytes[byteArrayIndex + 1],
                                    bytes[byteArrayIndex + 2]};
                    EncodingHelperChar EHC =
                            new EncodingHelperChar(completeByte);
                    maxArr[EHCharsIndex] = EHC;
                    EHCharsIndex++;
                    numEncodings++;
                    byteArrayIndex += 3;
                } else if (((int) leadingByte & 0b11111000) == 0b11110000){
                    // four bytes
                    byte[] completeByte = new byte[]
                            {leadingByte,
                                    bytes[byteArrayIndex + 1],
                                    bytes[byteArrayIndex + 2],
                                    bytes[byteArrayIndex + 3]};
                    EncodingHelperChar EHC =
                            new EncodingHelperChar(completeByte);
                    maxArr[EHCharsIndex] = EHC;
                    EHCharsIndex++;
                    numEncodings++;
                    byteArrayIndex += 4;
                }
                else {
                    System.out.println(input + "is an invalid UTF-8 encoding");
                    System.out.println(getUseMsg());
                    System.exit(1);
                }
            }
            EHCharArray = new EncodingHelperChar[numEncodings];
            for (int i = 0; i < numEncodings; i++) {
                EHCharArray[i] = maxArr[i];
            }
        }
        else { // stringLength is odd, so one byte is a single hex char
            System.out.println(input + " is an invalid byte input");
            System.out.println(getUseMsg());
        }
        return EHCharArray;
    }

    public String getUseMsg() {
        String usgMsg = "usage: "
                + "[<input>] [-i|--input <input type>] "
                + "[-o|--output <output type>]\n"
                + "Input is mandatory; input and output flags are optional\n"
                + "Default types are string input and summary output\n"
                + "Arguments in square brackets above can be "
                + "rearranged in any order\n"
                + "Input types: string, codepoint, utf8\n"
                + "No spaces between characters or UTF-8 bytes; optional spaces"
                + " between codepoints\n"
                + "Output types: summary, string, codepoint, utf8\n";
        return usgMsg;
    }
}
