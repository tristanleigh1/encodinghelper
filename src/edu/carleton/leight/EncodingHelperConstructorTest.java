package edu.carleton.leight;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by leight on 4/12/15.
 */
public class EncodingHelperConstructorTest {

    @Test
    public void testStringConstructor() throws Exception {
        EncodingHelperChar[] expectedEHCArr = new EncodingHelperChar[1];
        EncodingHelperChar testEHC = new EncodingHelperChar('a');
        expectedEHCArr[0] = testEHC;
        EncodingHelperConstructor EHCons = new EncodingHelperConstructor();
        EncodingHelperChar[] actualEHCArr = EHCons.stringConstructor("a");
        int actualCodePt = actualEHCArr[0].getCodePoint();
        int expectedCodePt = expectedEHCArr[0].getCodePoint();
        assertEquals(expectedCodePt, actualCodePt);

    }

    @Test
    public void testCodePointConstructor() throws Exception {
        EncodingHelperChar[] expectedEHCArr = new EncodingHelperChar[1];
        EncodingHelperChar testEHC = new EncodingHelperChar('a');
        expectedEHCArr[0] = testEHC;
        EncodingHelperConstructor EHCons = new EncodingHelperConstructor();
        EncodingHelperChar[] actualEHCArr = EHCons.codePointConstructor("0061");
        int actualCodePt = actualEHCArr[0].getCodePoint();
        int expectedCodePt = expectedEHCArr[0].getCodePoint();
        assertEquals(expectedCodePt, actualCodePt);
    }

    @Test
    public void testUtf8Constructor() throws Exception {
        EncodingHelperChar[] expectedEHCArr = new EncodingHelperChar[1];
        EncodingHelperChar testEHC = new EncodingHelperChar('a');
        expectedEHCArr[0] = testEHC;
        EncodingHelperConstructor EHCons = new EncodingHelperConstructor();
        EncodingHelperChar[] actualEHCArr = EHCons.utf8Constructor("\\x61");
        int actualCodePt = actualEHCArr[0].getCodePoint();
        int expectedCodePt = expectedEHCArr[0].getCodePoint();
        assertEquals(expectedCodePt, actualCodePt);
    }
}