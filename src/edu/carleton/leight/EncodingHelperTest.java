package edu.carleton.leight;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by leight on 4/12/15.
 */
public class EncodingHelperTest {

    @Test
    public void testGetSummaryOutputShouldReturnCorrectSummary() throws Exception {
        String expectedSummary = "Character: a\n" +
                "Code point: U+0061\n" +
                "Name: LATIN SMALL LETTER A\n" +
                "UTF8: \\x61\n";
        EncodingHelper EH = new EncodingHelper("a", "none", "none");
        String actualSummary = EH.getSummaryOutput();
        assertEquals(expectedSummary, actualSummary);
    }

    @Test
    public void testGetCodePointOutputShouldReturnCorrectCodePoint() throws Exception {
        String expectedCodePoint = "U+0061";
        EncodingHelper EH = new EncodingHelper("a", "string", "codepoint");
        String actualCodePoint = EH.getCodePointOutput();
        assertEquals(expectedCodePoint, actualCodePoint);
    }

    @Test
    public void testGetStringOutputShouldReturnCorrectString() throws Exception {
        String expectedString = "a";
        EncodingHelper EH = new EncodingHelper("U+0061", "codepoint", "string");
        String actualString = EH.getStringOutput();
        assertEquals(expectedString, actualString);
    }

    @Test
    public void testGetUTF8OutputShouldReturnCorrectUTF8String() throws Exception {
        String expectedUTF8 = "\\x61";
        EncodingHelper EH = new EncodingHelper("a", "none", "utf8");
        String actualUTF8 = EH.getUTF8Output();
        assertEquals(expectedUTF8, expectedUTF8);
    }

    @Test
    public void testMainInputAndOutputTypes() throws Exception {
        String expectedInputType = "codepoint";
        String expectedOutputType = "string";
        EncodingHelper EH = new EncodingHelper("U+0067", "codepoint", "string");
        String actualInputType = EH.getInputType();
        String actualOutputType = EH.getOutputType();
        assertEquals(expectedInputType, actualInputType);
        assertEquals(expectedOutputType, actualOutputType);
    }
}